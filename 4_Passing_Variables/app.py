# import datatypes, methods from flask framework
from flask import Flask, render_template, flash, redirect, request, session, abort
from random import randint

# create a Flask class's instance
app = Flask(__name__)

# this function mapped to the home '/' URL
@app.route("/")
def index():
    return "Flask App!"

# this function mapped to '/hello/name/' URL
@app.route('/hello/<string:name>/')
def hello(name):
    # pick random quote
    # an array of multiple quotes
    quotes = ["'If people do not believe that mathematics is simple, it is only because they do not realize how complicated life is.' -- John Louis von Neumann",
              "'Computer science is no more about computers than astronomy is about telescopes' -- Edsger Dijkstra ",
              "'To understand recursion you must first understand recursion..' -- Unknown",
              "'You look at things that are and ask, why? I dream of things that never were and ask, why not?' -- Unknown",
              "'Mathematics is the key and door to the sciences.' -- Galileo Galilei",
              "'Not everyone will understand your journey. Thats fine. Its not their journey to make sense of. Its yours.' -- Unknown"  ]
    # return a random number between 0 and (the total number of quotes - 1) (start counting from 0)
    randomNumber = randint(0, len(quotes) - 1)
    # set the quote to the quote the computer has chosen
    quote = quotes[randomNumber]

    return render_template('test.html', **locals())

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
