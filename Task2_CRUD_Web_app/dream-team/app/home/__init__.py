# app/home/__init__.py

from flask import Blueprint

# create a Blueprint object and initialize it with a name
home = Blueprint('home', __name__)

# import the views
from . import views
