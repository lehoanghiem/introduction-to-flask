# app/home/views.py

from flask import render_template
from flask_login import login_required

from . import home

# each view function has a decorator, which has a URL route as a parameter
# the homapage view renders the home template
@home.route('/')
def homepage():
    """
    Render the homepage template on the / route
    """
    return render_template('home/index.html', title='Welcome')

# the dashboard view renders the dashboard template
# the dashboard view has a decorator, meaning users must be logged in to access it
@home.route('/dashboard')
@login_required
def dashboard():
    """
    Render the dashboard template on the /dashboard route
    """
    return render_template('home/dashboard.html', title="Dashboard")
