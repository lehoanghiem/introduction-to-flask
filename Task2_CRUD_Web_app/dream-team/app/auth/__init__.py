# app/auth/__init__.py

from flask import Blueprint

# create a Blueprint object and initialize it with a name
auth = Blueprint('auth', __name__)

# import the views
from . import views
