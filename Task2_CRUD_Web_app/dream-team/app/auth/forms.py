# app/auth/forms.py

from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError
from wtforms.validators import DataRequired, Email, EqualTo

from ..models import Employee

class RegistrationForm(FlaskForm):
    """
    Form for users to create new account
    """
    # all the fields in the models have the 'DataRequired()' validator
    # meaning users will be required to fill all of them in order to register or logn.
    email = StringField('Email', validators=[DataRequired(), Email()])      # use 'Email()' validator
                                                                            # to ensure valid email formats are used
    username = StringField('Username', validators=[DataRequired()])
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    password = PasswordField('Password',
                             validators=[DataRequired(),
                                         EqualTo('confirm_password')])      # 'EqualTo()' validator to confirm
                                                                            # the 'password' and 'confirm_password'
                                                                            # fields are matched
    confirm_password = PasswordField('Confirm Password')
    # the 'submit' field as a button users will be able to click to register and login, respectively
    submit = SubmitField('Register')

    # method to ensure the email have not been used before
    def validate_email(self, field):
        if Employee.query.filter_by(email=field.data).first():
            raise ValidationError('Email is already in use.')

    # method to ensure the username have not been used before
    def validate_username(self, field):
        if Employee.query.filter_by(username=field.data).first():
            raise ValidationError('Username is already in use.')

class LoginForm(FlaskForm):
    """
    Form for users to login
    """
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = StringField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
