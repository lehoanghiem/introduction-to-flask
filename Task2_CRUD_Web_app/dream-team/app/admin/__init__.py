# app/admin/__init__.py

from flask import Blueprint

# create a Blueprint object and initialize it with a name
admin = Blueprint('admin', __name__)

# import the views
from . import views
