# app/__init__.py

# third-party imports
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# after existing third-party imports
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap

# local imports
from config import app_config

# db variable initialization
db = SQLAlchemy()
# after the db variable initialization
login_manager = LoginManager()

def create_app(config_name):
    # given a configuration name, loads the correct configuration from the
    # 'config.py' file, as well as the configuration from the 'instance/config.py' file.
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    # create a Bootstrap instance
    Bootstrap(app)
    # create a 'db' object which is used to interact with the database.
    db.init_app(app)

    # # temporary route
    # @app.route('/')
    # def hello_world():
    #     return 'Hello, World!'

    # initialization of the LoginManager object
    login_manager.init_app(app)
    # if a user tries to access a page that they are not authorized to, it will redirect
    # to the specified view and display the specified message
    # not design 'auth.login' view yet
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"

    # create a 'migrate' object which allow to run migrations using Flask-Migrate
    migrate = Migrate(app, db)

    # import the models from the 'app' package
    from app import models

    # register the blueprints on the app
    # import each blueprint object and register it
    from .admin import admin as admin_blueprint
    # for 'admin' blueprint, adding a url prefix
    # meaning all the views for this blueprint will be accessed in the browser with
    # the url prefix '/admin'
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    # import each blueprint object and register it
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # import each blueprint object and register it
    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    return app
