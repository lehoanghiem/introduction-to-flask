# import class datatypes, methods from 'flask' framework
from flask import Flask, flash, redirect, render_template, request, session, abort

# create a Flask class's instance
app = Flask(__name__)

# function mapped to the home '/' URL
@app.route("/")
def index():
    return "Flask App!"

# function mapped to the '/hello/name/' URL
@app.route("/hello/<string:name>/")
def hello(name):
    # this method 'render_template' will generate a template object out
    # of that HTML and return it to the browser when the user visits associated URL.
    # return render_template('test.html', name=name)
    return render_template('test_modified.html', name=name)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
