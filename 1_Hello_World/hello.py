# making available the code needed to build web apps with flask
# flask is the framework, Flask is a Python class datatype
from flask import Flask

# create an instance of the Flask class
# __name__ is a special variable getting as value the string "__main__"
app = Flask(__name__)

# define a function returning the string 'Hello World!'
# this function is mapped to the home '/' URL
@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
